import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MarkdownService } from 'ngx-markdown';
import { I18nTestingModule } from '../../shared/testing/i18n-testing/i18n-testing.module';

import { MotdModalComponent } from './motd-modal.component';

describe('MotdModalComponent', () => {
  let component: MotdModalComponent;
  let fixture: ComponentFixture<MotdModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [I18nTestingModule],
      declarations: [ MotdModalComponent ],
      providers: [
        NgbActiveModal,
        {
          provide: MarkdownService,
          useValue: {},
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MotdModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
