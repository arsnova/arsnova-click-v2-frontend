import {Component, OnInit} from '@angular/core';
import {IMotdEntity} from '../../lib/entities/motd/MotdEntity';
import {AdminApiService} from '../../service/api/admin/admin-api.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import {MarkdownService} from 'ngx-markdown';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-edit-motd-modal',
  templateUrl: './edit-motd-modal.component.html',
  styleUrls: ['./edit-motd-modal.component.scss'],
})
export class EditMotdModalComponent implements OnInit {
  public motd: IMotdEntity;
  public motdForm: FormGroup;
  public isSending = false;

  constructor(
      private adminApiService: AdminApiService,
      private fb: FormBuilder,
      private modal: NgbActiveModal,
      private markdownService: MarkdownService,
      private datePipe: DatePipe
  ) {
  }

  public ngOnInit(): void {
  }

  public submitForm(): void {
    this.isSending = true;
    for (const control of Object.values(this.motdForm.controls)) {
      control.markAsDirty();
      control.updateValueAndValidity();
      if (control.invalid) {
        console.log(control);
      }
    }

    if (this.motdForm.valid) {
      this.motd.title = this.motdForm.get('title').value;
      this.motd.content = this.motdForm.get('content').value;
      this.motd.isPinned = this.motdForm.get('isPinned').value;
      this.motd.expireAt = this.motdForm.get('expireAt').value;
      this.adminApiService.editMotd(this.motd).subscribe((motd) => {
        if (motd) {
          this.motd = motd;
          this.modal.close();
        } else {
        }
        this.isSending = false;
      });
    }
  }

  public createForm(): void {
    this.motdForm = this.fb.group(
        {
          title: [this.motd.title, Validators.required],
          content: [this.motd.content, Validators.required],
          isPinned: [this.motd.isPinned],
          expireAt: [this.datePipe.transform(this.motd.expireAt, 'yyyy-MM-dd'), Validators.required],
        },
        {updateOn: 'change'}
    );
  }

  public compileMarkdownContent(): string {
    return this.markdownService.compile(this.motdForm.get('content').value);
  }

  public isPinnedChange(status: boolean): void {
    if (status) {
      // Set pinned message to expire in 1000 days
      this.motdForm.get('expireAt').setValue(this.datePipe.transform(new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1000), 'yyyy-MM-dd'));
    } else {
      this.motdForm.get('expireAt').setValue(this.datePipe.transform(this.motd.expireAt, 'yyyy-MM-dd'));
    }
  }
}
