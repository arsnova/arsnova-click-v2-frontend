import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PLATFORM_ID } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MarkdownService } from 'ngx-markdown';
import { jwtOptionsFactory } from '../../lib/jwt.factory';
import { I18nTestingModule } from '../../shared/testing/i18n-testing/i18n-testing.module';

import { EditMotdModalComponent } from './edit-motd-modal.component';

describe('EditMotdModalComponent', () => {
  let component: EditMotdModalComponent;
  let fixture: ComponentFixture<EditMotdModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, I18nTestingModule, HttpClientTestingModule, JwtModule.forRoot({
        jwtOptionsProvider: {
          provide: JWT_OPTIONS,
          useFactory: jwtOptionsFactory,
          deps: [PLATFORM_ID],
        },
      })],
      providers: [
        NgbActiveModal,
        {
          provide: MarkdownService,
          useValue: {},
        },
        DatePipe,
      ],
      declarations: [ EditMotdModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMotdModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
