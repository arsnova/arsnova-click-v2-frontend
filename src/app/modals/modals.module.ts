import { NgModule } from '@angular/core';
import { MarkdownModule } from 'ngx-markdown';
import { SharedModule } from '../shared/shared.module';
import { AddModeComponent } from './add-mode/add-mode.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AvailableQuizzesComponent } from './available-quizzes/available-quizzes.component';
import { HotkeyCheatsheetComponent } from './hotkey-cheatsheet/hotkey-cheatsheet.component';
import { OutdatedVersionComponent } from './outdated-version/outdated-version.component';
import { QuizSaveComponent } from './quiz-save/quiz-save.component';
import { ServerUnavailableModalComponent } from './server-unavailable-modal/server-unavailable-modal.component';
import { MotdModalComponent } from './motd-modal/motd-modal.component';
import { EditMotdModalComponent } from './edit-motd-modal/edit-motd-modal.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    SharedModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AvailableQuizzesComponent,
    AddModeComponent,
    AddUserComponent,
    QuizSaveComponent,
    ServerUnavailableModalComponent,
    HotkeyCheatsheetComponent,
    OutdatedVersionComponent,
    MotdModalComponent,
    EditMotdModalComponent,
  ],
  exports: [
    HotkeyCheatsheetComponent,
  ],
  bootstrap: []
})
export class ModalsModule {
}
