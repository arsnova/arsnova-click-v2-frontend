import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { QuizService } from '../quiz/quiz.service';
import { CountdownService } from './countdown.service';
import { TimeService } from '../time/time.service';
import { SimpleMQ } from 'ng2-simple-mq';
import { of } from 'rxjs';
import { QuizEntity } from '../../lib/entities/QuizEntity';
import { CountdownState } from '../../lib/enums/Countdown';
import { MessageProtocol } from '../../lib/enums/Message';
import { AbstractQuestionEntity } from '../../lib/entities/question/AbstractQuestionEntity';

describe('CountdownService', () => {
  let countdownService: CountdownService;
  let timeServiceSpy: jasmine.SpyObj<TimeService>;
  let simpleMQSpy: jasmine.SpyObj<SimpleMQ>;
  let quizServiceSpy: jasmine.SpyObj<QuizService>;

  beforeEach(() => {
    const timeSpy = jasmine.createSpyObj('TimeService', ['getTime', 'syncTime']);
    const mqSpy = jasmine.createSpyObj('SimpleMQ', ['publish']);
    const quizSpy = jasmine.createSpyObj('QuizService', ['quiz', 'currentQuestion'], {
      quizUpdateEmitter: of<QuizEntity | undefined>(),
    });
    TestBed.configureTestingModule({
      providers: [
        CountdownService,
        { provide: TimeService, useValue: timeSpy },
        { provide: SimpleMQ, useValue: mqSpy },
        { provide: QuizService, useValue: quizSpy },
      ],
    });
    countdownService = TestBed.inject(CountdownService);
    timeServiceSpy = TestBed.inject(TimeService) as jasmine.SpyObj<TimeService>;
    simpleMQSpy = TestBed.inject(SimpleMQ) as jasmine.SpyObj<SimpleMQ>;
    quizServiceSpy = TestBed.inject(QuizService) as jasmine.SpyObj<QuizService>;
  });

  it('should be created', () => {
    expect(countdownService).toBeTruthy();
  });

  describe('Start Countdown', () => {
    it('Should start the countdown if there is a quiz, there is a question and its started', () => {
      const mockQuestion = { timer: 60 } as AbstractQuestionEntity;
      quizServiceSpy.currentQuestion.and.returnValue(mockQuestion);
      quizServiceSpy.quiz = { currentStartTimestamp: 100 } as QuizEntity;

      countdownService['calculateRemainingCountdown'] = () => { return 60 };
      countdownService['execCountdown'] = () => {};
      spyOn<any>(countdownService, 'calculateRemainingCountdown');
      spyOn<any>(countdownService, 'execCountdown');

      const returnValue = countdownService.startCountdown();

      expect(countdownService['calculateRemainingCountdown']).toHaveBeenCalledTimes(1);
      expect(countdownService['execCountdown']).toHaveBeenCalledTimes(1);
      expect(returnValue).toBe(CountdownState.Running);
      expect(countdownService.countdownState).toBe(CountdownState.Running);
    });

    it('Shouldn\'t start the countdown if there is no quiz', () => {
      quizServiceSpy.quiz = undefined;

      countdownService['calculateRemainingCountdown'] = () => { return 60 };
      countdownService['execCountdown'] = () => {};
      spyOn<any>(countdownService, 'calculateRemainingCountdown');
      spyOn<any>(countdownService, 'execCountdown');

      const returnValue = countdownService.startCountdown();

      expect(countdownService['calculateRemainingCountdown']).toHaveBeenCalledTimes(0);
      expect(countdownService['execCountdown']).toHaveBeenCalledTimes(0);
      expect(returnValue).toBe(CountdownState.Stopped);
      expect(countdownService.countdownState).toBe(CountdownState.Stopped);
    });

    it('Shouldn\'t start the countdown if there is no question', () => {
      quizServiceSpy.currentQuestion.and.returnValue(undefined);
      quizServiceSpy.quiz = { currentStartTimestamp: 100 } as QuizEntity;

      countdownService['calculateRemainingCountdown'] = () => { return 60 };
      countdownService['execCountdown'] = () => {};
      spyOn<any>(countdownService, 'calculateRemainingCountdown');
      spyOn<any>(countdownService, 'execCountdown');

      const returnValue = countdownService.startCountdown();

      expect(countdownService['calculateRemainingCountdown']).toHaveBeenCalledTimes(0);
      expect(countdownService['execCountdown']).toHaveBeenCalledTimes(0);
      expect(returnValue).toBe(CountdownState.Stopped);
      expect(countdownService.countdownState).toBe(CountdownState.Stopped);
    });

    it('Shouldn\'t start the countdown if the quiz isn\'t started', () => {
      const mockQuestion = { timer: 60 } as AbstractQuestionEntity;
      quizServiceSpy.currentQuestion.and.returnValue(mockQuestion);
      quizServiceSpy.quiz = { currentStartTimestamp: -1 } as QuizEntity;

      countdownService['calculateRemainingCountdown'] = () => { return 60 };
      countdownService['execCountdown'] = () => {};
      spyOn<any>(countdownService, 'calculateRemainingCountdown');
      spyOn<any>(countdownService, 'execCountdown');

      const returnValue = countdownService.startCountdown();

      expect(countdownService['calculateRemainingCountdown']).toHaveBeenCalledTimes(0);
      expect(countdownService['execCountdown']).toHaveBeenCalledTimes(0);
      expect(returnValue).toBe(CountdownState.Stopped);
      expect(countdownService.countdownState).toBe(CountdownState.Stopped);
    });

    it('Shouldn\'t start the countdown if there is no time left', () => {
      const mockQuestion = { timer: 60 } as AbstractQuestionEntity;
      quizServiceSpy.currentQuestion.and.returnValue(mockQuestion);
      quizServiceSpy.quiz = { currentStartTimestamp: 0 } as QuizEntity;

      countdownService['calculateRemainingCountdown'] = () => { return 60 };
      countdownService['execCountdown'] = () => {};
      spyOn<any>(countdownService, 'calculateRemainingCountdown');
      spyOn<any>(countdownService, 'execCountdown');

      const returnValue = countdownService.startCountdown();

      expect(countdownService['calculateRemainingCountdown']).toHaveBeenCalledTimes(0);
      expect(countdownService['execCountdown']).toHaveBeenCalledTimes(0);
      expect(returnValue).toBe(CountdownState.Stopped);
      expect(countdownService.countdownState).toBe(CountdownState.Stopped);
    });
  });

  describe('Pause Countdown', () => {
    it('Should pause the countdown if it is running', () => {
      countdownService['_countdownState'] = CountdownState.Running;
      countdownService['_intervalID'] = setInterval(() => {}, 1000);

      const returnValue = countdownService.pauseCountdown();

      expect(returnValue).toBe(CountdownState.Paused);
      expect(countdownService['_intervalID']).toBe(undefined);
      expect(countdownService['_countdownState']).toBe(CountdownState.Paused);
    });

    it('Shouldn\'t pause the countdown if it is stopped', () => {
      countdownService['_countdownState'] = CountdownState.Stopped;

      const returnValue = countdownService.pauseCountdown();

      expect(returnValue).toBe(CountdownState.Stopped);
      expect(countdownService['_countdownState']).toBe(CountdownState.Stopped);
    });
  });

  describe('Stop Countdown', () => {
    it('Should stop the countdown if it is running', () => {
      countdownService['_countdownState'] = CountdownState.Running;
      countdownService['clearCountdown'] = () => {};
      spyOn<any>(countdownService, 'clearCountdown');

      countdownService.stopCountdown();

      expect(countdownService['clearCountdown']).toHaveBeenCalledTimes(1);
      expect(countdownService['_countdownState']).toBe(CountdownState.Stopped);
    });

    it('Should stop the countdown if it is paused', () => {
      countdownService['_countdownState'] = CountdownState.Paused;
      countdownService['clearCountdown'] = () => {};
      spyOn<any>(countdownService, 'clearCountdown');

      countdownService.stopCountdown();

      expect(countdownService['clearCountdown']).toHaveBeenCalledTimes(1);
      expect(countdownService['_countdownState']).toBe(CountdownState.Stopped);
    });
  });

  describe('Calculate Remaining Countdown', () => {
    it('Should calculate the remaining countdown if the quiz is started and there is a question and the time is synced', () => {
      const mockQuestion = { timer: 60 } as AbstractQuestionEntity;
      quizServiceSpy.currentQuestion.and.returnValue(mockQuestion);
      quizServiceSpy.quiz = { currentStartTimestamp: 10000 } as QuizEntity;
      timeServiceSpy.getTime.and.returnValue(11000);

      const returnValue = countdownService['calculateRemainingCountdown']();

      expect(returnValue).toBe(59);
      expect(countdownService['_countdown']).toBe(59);
    });

    it('Should calculate the remaining countdown if the quiz is started and there is a question and the time is synced and round by ceil method', () => {
      const mockQuestion = { timer: 60 } as AbstractQuestionEntity;
      quizServiceSpy.currentQuestion.and.returnValue(mockQuestion);
      quizServiceSpy.quiz = { currentStartTimestamp: 9001 } as QuizEntity;
      timeServiceSpy.getTime.and.returnValue(11000);

      const returnValue = countdownService['calculateRemainingCountdown']();

      expect(returnValue).toBe(59);
      expect(countdownService['_countdown']).toBe(59);
    });

    it('Shouldn\'t calculate the remaining countdown if there is no question', () => {
      quizServiceSpy.currentQuestion.and.returnValue(undefined);
      quizServiceSpy.quiz = { currentStartTimestamp: 10000 } as QuizEntity;

      const returnValue = countdownService['calculateRemainingCountdown']();

      expect(returnValue).toBe(-1);
      expect(countdownService['_countdown']).toBe(0);
    });

    it('Shouldn\'t calculate the remaining countdown if the quiz isn\'t started', () => {
      const mockQuestion = { timer: 60 } as AbstractQuestionEntity;
      quizServiceSpy.currentQuestion.and.returnValue(mockQuestion);
      quizServiceSpy.quiz = { currentStartTimestamp: -1 } as QuizEntity;

      const returnValue = countdownService['calculateRemainingCountdown']();

      expect(returnValue).toBe(-1);
      expect(countdownService['_countdown']).toBe(0);
    });

    it('Shouldn\'t calculate the remaining countdown if the quiz has no start time', () => {
      const mockQuestion = { timer: 60 } as AbstractQuestionEntity;
      quizServiceSpy.currentQuestion.and.returnValue(mockQuestion);
      quizServiceSpy.quiz = { } as QuizEntity;

      const returnValue = countdownService['calculateRemainingCountdown']();

      expect(returnValue).toBe(-1);
      expect(countdownService['_countdown']).toBe(0);
    });

    it('Should calculate the remaining countdown if the quiz is started and there is a question and round by ceil method and sync the time if it isnt synced', () => {
      const mockQuestion = { timer: 60 } as AbstractQuestionEntity;
      quizServiceSpy.currentQuestion.and.returnValue(mockQuestion);
      quizServiceSpy.quiz = { currentStartTimestamp: 10000 } as QuizEntity;
      timeServiceSpy.getTime.and.returnValue(-1);
      timeServiceSpy.syncTime.and.returnValue(of((() => {
        timeServiceSpy.getTime.and.returnValue(11000); 
        return 11000;
      })()));

      const returnValue = countdownService['calculateRemainingCountdown']();

      expect(returnValue).toBe(59);
      expect(countdownService['_countdown']).toBe(59);
    });
  });

  describe('Exec Countdown', () => {
    describe('Should stop the countdown if it is less or equal to 0', () => {
      it('Should stop the countdown if it is 0', () => {
        countdownService['_countdown'] = 0;
        countdownService['_countdownState'] = CountdownState.Running;
        countdownService['stopCountdown'] = () => countdownService['_countdownState'] = CountdownState.Stopped;

        countdownService['execCountdown']();

        expect(countdownService['_countdownState']).toBe(CountdownState.Stopped);
      });

      it('Should stop the countdown if it is highest possible negativ number', () => {
        countdownService['_countdown'] = Number.MIN_SAFE_INTEGER;
        countdownService['_countdownState'] = CountdownState.Running;
        countdownService['stopCountdown'] = () => countdownService['_countdownState'] = CountdownState.Stopped;

        countdownService['execCountdown']();

        expect(countdownService['_countdownState']).toBe(CountdownState.Stopped);
      });
    });

    it('Should do nothing if an interval is set and the countdown is greater than 0', () => {
      countdownService['_countdownState'] = CountdownState.Stopped;
      countdownService['_countdown'] = 60;
      const intervalID = setInterval(() => {}, 1000);
      countdownService['_intervalID'] = intervalID;

      countdownService['execCountdown']();

      expect(countdownService['_countdown']).toBe(60);
      expect(countdownService['_countdownState']).toBe(CountdownState.Stopped);
      expect(countdownService['_intervalID']).toBe(intervalID);
    });

    it('Should set the countdown down by 1 every second until its 0 and publishing it to the messageQueue every time', fakeAsync(() => {
      countdownService['_countdown'] = 10;
      countdownService['stopCountdown'] = () => {};
      spyOn(countdownService, 'stopCountdown');

      countdownService['execCountdown']();

      tick(3000);

      expect(simpleMQSpy.publish).toHaveBeenCalledTimes(4);
      expect(simpleMQSpy.publish.calls.mostRecent().args[0]).toBe(MessageProtocol.Countdown);
      expect(simpleMQSpy.publish.calls.mostRecent().args[1]).toEqual({ value: 7 });

      tick(7000);

      expect(simpleMQSpy.publish).toHaveBeenCalledTimes(10);
      expect(countdownService['stopCountdown']).toHaveBeenCalledTimes(1);
      expect(simpleMQSpy.publish.calls.mostRecent().args[0]).toBe(MessageProtocol.Countdown);
      expect(simpleMQSpy.publish.calls.mostRecent().args[1]).toEqual({ value: 1 });

      clearInterval(countdownService['_intervalID']);
    }));
  });

  describe('Clear Countdown', () => {
    it('Should clear the countdown information', () => {
      countdownService['_intervalID'] = setInterval(() => {}, 1000);

      countdownService['clearCountdown']();

      expect(countdownService['_intervalID']).toBe(undefined);
      expect(countdownService['_countdown']).toBe(0);
      expect(simpleMQSpy.publish).toHaveBeenCalledTimes(1);
      expect(simpleMQSpy.publish.calls.mostRecent().args[0]).toBe(MessageProtocol.Countdown);
      expect(simpleMQSpy.publish.calls.mostRecent().args[1]).toEqual({ value: 0 });
    });
  });
});
