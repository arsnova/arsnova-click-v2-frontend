import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { TimeService } from '../time/time.service';
import { SimpleMQ } from 'ng2-simple-mq';
import { MessageProtocol } from '../../lib/enums/Message';
import { CountdownState } from '../../lib/enums/Countdown';
import { QuizService } from '../quiz/quiz.service';
import { QuizState } from '../../lib/enums/QuizState';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { QuizEntity } from '../../lib/entities/QuizEntity';

@Injectable({
  providedIn: 'root'
})
export class CountdownService {
  private _countdown: number = 0;
  private _intervalID: NodeJS.Timeout | undefined;
  private _countdownState: CountdownState = CountdownState.Stopped;

  get countdownState(): CountdownState {
    return this._countdownState;
  }

  constructor(    
    @Inject(PLATFORM_ID) private platformId: Object,
    private timeService: TimeService,
    private messageQueue: SimpleMQ,
    private quizService: QuizService,
  ) { 
    if(isPlatformBrowser(platformId)) {
      this.initCountdownForQuizUpdateEmitter().subscribe();
    }
  }

  /**
   * Creates an Observable which, adds the {@link startCountdown} Function to be executed when the {@link quizUpdateEmitter} is fired.
   * @returns {Observable<void | QuizEntity>}The created Observable.
   */
  private initCountdownForQuizUpdateEmitter(): Observable<void | QuizEntity> {
    return this.quizService.quizUpdateEmitter.pipe(tap(quiz => {
      if (quiz && !(this.quizService.quiz.state === QuizState.Inactive)) {
        this.startCountdown();
      }
    }));
  }

  /**
   * Starts the countdown for the current question in the quiz, if not already running and if the quiz, 
   * the current question, and the quiz start timestamp are set.
   * @returns {CountdownState} The countdown state.
   */
  public startCountdown(): CountdownState {
    if(!this.quizService.quiz 
      || !this.quizService.currentQuestion() 
      || this.quizService.quiz.currentStartTimestamp <= 0
      || this._countdownState === CountdownState.Running) {
      return this._countdownState;
    }

    this.calculateRemainingCountdown();
    console.log("Started Countdown");
    this._countdownState = CountdownState.Running;
    this.execCountdown();
    return this._countdownState;
  }

  /**
   * Pauses the Countdown if the Quiz isn't stopped or paused already.
   * @returns {CountdownState} The countdown state.
   */
  public pauseCountdown(): CountdownState {
    if(this._countdownState === CountdownState.Paused
      || this._countdownState === CountdownState.Stopped) {
      return this._countdownState;
    }
    clearInterval(this._intervalID);
    this._intervalID = undefined;
    console.log("Paused Countdown");
    this._countdownState = CountdownState.Paused;
    return this._countdownState;
  }

  /**
   * Stops the Countdown if the Quiz isn't stopped already.
   */
  public stopCountdown(): void {
    if(this._countdownState === CountdownState.Stopped) {
      return;
    }
    this.clearCountdown();
    console.log("Stopped Countdown");
    this._countdownState = CountdownState.Stopped;
  }

  /**
   * Calculates the remaining countdown based on the starting time of the question and its duration.
   * @returns {number} The remaining countdown. Returns -1 if the startTimeStamp isn't set or there is no current question.
   */
  private calculateRemainingCountdown(): number {
    console.log("CD [CurQues]: ", this.quizService.currentQuestion());
    console.log("CD [CurTS]: ", this.quizService.quiz.currentStartTimestamp);
    if (!this.quizService.quiz.currentStartTimestamp
      || !this.quizService.currentQuestion()
      || this.quizService.quiz.currentStartTimestamp < 0) {
        return -1;
    }
    if(this.timeService.getTime() === -1) {
      this.timeService.syncTime().subscribe(() => {
        this._countdown = Math.ceil((this.quizService.quiz.currentStartTimestamp  - this.timeService.getTime()) / 1000) + this.quizService.currentQuestion().timer;
        return this._countdown;
      });
    } else {
      this._countdown = Math.ceil((this.quizService.quiz.currentStartTimestamp  - this.timeService.getTime()) / 1000) + this.quizService.currentQuestion().timer;
      console.log("CD [cd]: ", this._countdown);
      return this._countdown;
    }
  }

  /**
   * Executes the interval for the countdown.
   */
  private execCountdown(): void {
    if (this._countdown <= 0) {
      this.stopCountdown();
      return;
    }

    if (!this._intervalID) {
      this.messageQueue.publish(MessageProtocol.Countdown, {value: this._countdown});
      console.log("Countdown: ", this._countdown);
      this._intervalID = setInterval(() => {
        --this._countdown;
        if (this._countdown <= 0) {
          this.stopCountdown();
        } else {
          this.messageQueue.publish(MessageProtocol.Countdown, {value: this._countdown});
          console.log("Countdown: ", this._countdown);
        }
      }, 1000);
    }
  }

  /**
   * Clears the countdown interval and puts the countdown to 0.
   */
  private clearCountdown(): void {
    clearInterval(this._intervalID);
    this._intervalID = undefined;
    this._countdown = 0;
    this.messageQueue.publish(MessageProtocol.Countdown, {value: this._countdown});
    console.log("Countdown: ", this._countdown);
  }
}
