import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { TimeService } from './time.service';
import { defer, of } from 'rxjs';

describe('TimeService', () => {
  let timeService: TimeService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TimeService],
    });
    timeService = TestBed.inject(TimeService);
  }));

  it('should be created', () => {
    expect(timeService).toBeTruthy();
  });

  describe('initSyncTime', () => {
    it('Should initialize the time sync by syncing the time six times sequentially.', () => {
      let timesCalled = 0;
      timeService['syncTime'] = jasmine.createSpy().and.callFake(() => {
        timesCalled += 100;
        return of(timesCalled);
      });

      let result: number[] = [];

      timeService['initTimeSync']().subscribe(value => {
        result.push(value);
      });

      const expectation = defer(() => {
        expect(timeService['syncTime']).toHaveBeenCalledTimes(6);
        expect(result[0]).toBe(100);
        expect(result[1]).toBe(200);
        expect(result[2]).toBe(300);
        expect(result[3]).toBe(400);
        expect(result[4]).toBe(500);
        expect(result[5]).toBe(600);
      });

      expectation.subscribe();
    });
  });

  describe('syncTime', () => {
    let http: HttpTestingController;

    beforeEach(() => {
      http = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
      http.expectOne('http://localhost:3010/api/v1/time');
      http.verify();
    });

    it('Should calculate the current time delta of the client and the server and add it to the time deltas array', () => {
      const timeRequestSend = 1000;
      const timeRequestReceived = 2142;
      const serverTime = 3862;
      const halfRTT = 571;
      const timeDelta = 2291;

      expect(timeRequestReceived + timeDelta).toBe(serverTime + halfRTT);

      spyOn(Date, 'now').and.returnValue(timeRequestSend);
      spyOn<any>(timeService, 'calculateAverageTimeDelta').and.returnValue(timeDelta);

      const syncTimeObservable = timeService.syncTime();
      Date.now = jasmine.createSpy().and.returnValue(timeRequestReceived);

      syncTimeObservable.subscribe(result => {
        expect(result).toBe(serverTime);
        expect(timeService['calculateAverageTimeDelta']).toHaveBeenCalledTimes(1);
        expect(timeService['timeDeltas'].length).toBe(1);
        expect(timeService['timeDeltas'][0]).toBe(timeDelta);
      });

      let req = http.match(req => req.url === 'http://localhost:3010/api/v1/time')[0];
      req.flush(serverTime);
    });

    it('Should calculate the current time delta of the client and the server and add it to the time deltas array. If the array has a length of 5 remove the first element.', () => {
      const timeRequestSend = 1000;
      const timeRequestReceived = 1400;
      const serverTime = 1400;
      const halfRTT = 200;
      const timeDelta = 200;
      timeService['timeDeltas'] = [100, 110, 90, 80, 120];

      expect(timeRequestReceived + timeDelta).toBe(serverTime + halfRTT);

      spyOn(Date, 'now').and.returnValue(timeRequestSend);
      spyOn<any>(timeService, 'calculateAverageTimeDelta').and.returnValue(timeDelta);

      const syncTimeObservable = timeService.syncTime();
      Date.now = jasmine.createSpy().and.returnValue(timeRequestReceived);

      syncTimeObservable.subscribe(result => {
        expect(result).toBe(serverTime);
        expect(timeService['calculateAverageTimeDelta']).toHaveBeenCalledTimes(1);
        expect(timeService['timeDeltas'].length).toBe(5);
        expect(timeService['timeDeltas'][0]).toBe(110);
        expect(timeService['timeDeltas'][5]).toBe(timeDelta);
      });

      let req = http.match(req => req.url === 'http://localhost:3010/api/v1/time')[0];
      req.flush(serverTime);
    });
  });

  describe('calculateAverageTimeDelta', () => {
    it('should calculate the average time delta based on the time deltas array', () => {
      timeService['timeDeltas'] = [100, 120, 140, 150, 155];
      expect(timeService['calculateAverageTimeDelta']()).toBe(133);

      timeService['timeDeltas'] = [-100, 0, 100];
      expect(timeService['calculateAverageTimeDelta']()).toBe(0);
    });

    it('should return -1 if the time deltas array is empty', () => {
      timeService['timeDeltas'] = [];
      expect(timeService['calculateAverageTimeDelta']()).toBe(-1);
    });
  });

  describe('getTime', () => {
    it('should return the current time adjusted by the average time delta', () => {
      const currentTime = Date.now();
      const averageTimeDelta = 50;
      const expectedTime = currentTime + averageTimeDelta;
      spyOn(Date, 'now').and.returnValue(currentTime);
  
      timeService['averageTimeDelta'] = averageTimeDelta;
  
      expect(timeService.getTime()).toEqual(expectedTime);
    });
  
    it('should return -1 if the average time delta is null', () => {
      timeService['averageTimeDelta'] = null;
  
      expect(timeService.getTime()).toEqual(-1);
    });
  });
});

