import { isPlatformBrowser } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { concat, Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators/tap';
import { DefaultSettings } from '../../lib/default.settings';

@Injectable({
  providedIn: 'root'
})
export class TimeService {
  // Time difference between Server and Client in Milliseconds
  private averageTimeDelta: number = null;
  private timeDeltas: number[] = [];

  constructor(    
    @Inject(PLATFORM_ID) private platformId: Object,
    private http: HttpClient,
  ) { 
    if(isPlatformBrowser(platformId)) {
      this.initTimeSync().subscribe(() => console.log("TimeSync Init"));
    }
  }

  /**
   * Initializes the time synchronization process by performing a series of time synchronization events.
   * @returns {Observable<number>} An Observable that emits the result of each time synchronization event.
   */
  private initTimeSync(): Observable<number> {
    return concat(this.syncTime(), this.syncTime(), this.syncTime(), this.syncTime(), this.syncTime(), this.syncTime());
  }

  /**
  Sends an HTTP request to the server to get the current server time, calculates the RTT and calculates the average time delta.
  @returns {Observable<number>} An Observable of the serverTime, which executes the HTTP-Request with it's functionality.
  */
  public syncTime(): Observable<number> {
    const timeRequestSend: number = Date.now();

    return this.http.get<number>(`${DefaultSettings.httpApiEndpoint}/time`).pipe(tap((serverTime: number) => {
      const timeRequestReceived: number = Date.now();
      const halfRTT: number = (timeRequestReceived - timeRequestSend) / 2;

      const currentDelta: number = serverTime + halfRTT - Date.now();
      console.log("Current: ", currentDelta);

      this.timeDeltas.push(currentDelta);
      if (this.timeDeltas.length > 5) this.timeDeltas.shift();

      this.calculateAverageTimeDelta();

      console.log("AD: ", this.averageTimeDelta);
      console.log("Server: ", new Date(serverTime));
      console.log("Sync: ", new Date(this.getTime()));
    }));
  }

  /**
  * Calculates the average time delta based on the time deltas array.
  * @returns {number} The average time delta as a number. Returns -1 if the time deltas array is empty.
  */
  private calculateAverageTimeDelta(): number {
    if(this.timeDeltas.length <= 0) {
      return -1;
    }

    const sumTimeDeltas: number = this.timeDeltas.reduce((sum, timeDelta) => sum + timeDelta, 0);

    this.averageTimeDelta = sumTimeDeltas / this.timeDeltas.length;

    return this.averageTimeDelta;
  }

  /**
  * Gets the current time adjusted by the average time delta.
  * @returns {number} The current time adjusted by the average time delta as a number. Returns -1 if the average time delta is null.
  */
  public getTime(): number {
    if (this.averageTimeDelta === null) {
      return -1;
    }

    return Date.now() + this.averageTimeDelta;
  }
}
