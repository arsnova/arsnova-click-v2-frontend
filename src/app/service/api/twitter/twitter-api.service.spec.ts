import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SimpleMQ } from 'ng2-simple-mq';
import { MarkdownService, MarkedOptions } from 'ngx-markdown';
import { I18nTestingModule } from '../../../shared/testing/i18n-testing/i18n-testing.module';
import { TwitterApiService } from './twitter-api.service';

describe('TwitterApiService', () => {
  let service: TwitterApiService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [I18nTestingModule, FormsModule, RouterTestingModule, HttpClientTestingModule, FontAwesomeModule, NgbModule],
      providers: [
        TwitterApiService, SimpleMQ, MarkdownService, HttpClient, {
          provide: MarkedOptions,
          useValue: {},
        },
      ],
    });
    service = TestBed.inject(TwitterApiService);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
