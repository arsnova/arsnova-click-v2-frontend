import { QuestionType } from '../../enums/QuestionType';
import { IValidationStackTrace } from '../../interfaces/IValidationStackTrace';
import { AbstractAnswerEntity } from '../answer/AbstractAnswerEntity';
import { DefaultAnswerEntity } from '../answer/DefaultAnswerEntity';

export abstract class AbstractQuestionEntity {
  public answerOptionList: Array<AbstractAnswerEntity>;
  public questionText: string;
  public timer: number;
  public displayAnswerText: boolean;
  public tags: Array<string>;
  public requiredForToken: boolean;
  public difficulty;
  public abstract TYPE: QuestionType;

  protected constructor(settingsService, data) {
    this.questionText = settingsService.frontEnv?.defaultQuizSettings.question.questionText;
    this.timer = settingsService.frontEnv?.defaultQuizSettings.question.timer;
    this.displayAnswerText = settingsService.frontEnv?.defaultQuizSettings.question.dispayAnswerText;
    this.tags = settingsService.frontEnv?.defaultQuizSettings.question.tags;
    this.requiredForToken = settingsService.frontEnv?.defaultQuizSettings.question.requiredForToken;
    this.difficulty = settingsService.frontEnv?.defaultQuizSettings.question.difficulty;
    this.questionText = data.questionText ? data.questionText : this.questionText;
    this.timer = typeof data.timer === 'number' ? data.timer : parseInt(data.timer ?? this.timer, 10);
    this.displayAnswerText = data.displayAnswerText ?? this.displayAnswerText;
    this.answerOptionList = data.answerOptionList ?? [];
    this.tags = data.tags ?? [];
    this.difficulty = data.difficulty ?? this.difficulty;

    this.requiredForToken = data.requiredForToken ?? this.requiredForToken;
  }

  public abstract translationReferrer(): string;

  public isValid(): boolean {
    let answerOptionListValid = true;
    this.answerOptionList.forEach(answerOption => {
      if (!answerOption.isValid()) {
        answerOptionListValid = false;
      }
    });
    const questionTextWithoutMarkdownChars = this.getQuestionTextWithoutMarkdownChars().length;

    // hard coded checkup values are ugly, but the schema import seems to be messed up here...
    return answerOptionListValid && questionTextWithoutMarkdownChars > 4 && questionTextWithoutMarkdownChars < 50001 && this.timer >= -1;
  }

  public getQuestionTextWithoutMarkdownChars(): string {
    return this.questionText.replace(/#/g, '').replace(/\*/g, '').replace(/1./g, '').replace(/\[/g, '').replace(/\]\(/g, '')
    .replace(/\)/g, '').replace(/- /g, '').replace(/ /g, '').replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/$/g, '')
    .replace(/<hlcode>/g, '').replace(/<\/hlcode>/g, '').replace(/>/g, '');
  }

  public getValidationStackTrace(): Array<IValidationStackTrace> {
    let result = Array<IValidationStackTrace>();
    const questionTextWithoutMarkdownChars = this.getQuestionTextWithoutMarkdownChars().length;
    if (questionTextWithoutMarkdownChars < 5) {
      result.push({
        occurredAt: { type: 'question' },
        reason: 'component.quiz_summary.validation_errors.reasons.question_text_too_small',
      });
    } else if (questionTextWithoutMarkdownChars > 50000) {
      result.push({
        occurredAt: { type: 'question' },
        reason: 'component.quiz_summary.validation_errors.reasons.question_text_too_long',
      });
    }
    if (this.timer < -1) {
      result.push({
        occurredAt: { type: 'question' },
        reason: 'component.quiz_summary.validation_errors.reasons.timer_too_small',
      });
    }
    this.answerOptionList.forEach(answerOption => {
      if (!answerOption.isValid()) {
        result = result.concat(answerOption.getValidationStackTrace());
      }
    });
    return result;
  }

  public equals(question: AbstractQuestionEntity): boolean {
    const questionAnswerOptionList = question.answerOptionList;
    if (questionAnswerOptionList.length === this.answerOptionList.length) {
      let isEqual = true;
      for (let i = 0; i < this.answerOptionList.length; i++) {
        if (isEqual && !this.answerOptionList[i].equals(questionAnswerOptionList[i])) {
          isEqual = false;
        }
      }
      if (question.timer !== this.timer || question.questionText !== this.questionText) {
        isEqual = false;
      }
      return isEqual;
    }
  }

  public addDefaultAnswerOption(settingsService, index = -1): void {
    if (index === -1 || index >= this.answerOptionList.length) {
      index = this.answerOptionList.length;
    }
    this.addAnswerOption(new DefaultAnswerEntity(settingsService, {
      answerText: '',
      isCorrect: false,
    }), index);
  }

  public addAnswerOption(answerOption: AbstractAnswerEntity, index: number = -1): void {
    if (index < 0 || index >= this.answerOptionList.length) {
      this.answerOptionList.push(answerOption);
    } else {
      this.answerOptionList.splice(index, 0, answerOption);
    }
  }

  public removeAnswerOption(index: number): void {
    if (index < 0 || index > this.answerOptionList.length) {
      throw new Error('Invalid argument for Question.removeAnswerOption');
    }
    this.answerOptionList.splice(index, 1);
  }

  public removeAllAnswerOptions(): void {
    this.answerOptionList.splice(0, this.answerOptionList.length);
  }
}
