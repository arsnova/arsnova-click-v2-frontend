export function uniqueArrayElements<T>(haystack: Array<T>): Array<T> {
  return haystack.filter(((value, index) => haystack.indexOf(value) === index));
}
