import { TranslateService } from '@ngx-translate/core';
import { DefaultAnswerEntity } from './entities/answer/DefaultAnswerEntity';
import { FreeTextAnswerEntity } from './entities/answer/FreetextAnwerEntity';
import { ABCDSurveyQuestionEntity } from './entities/question/ABCDSurveyQuestionEntity';
import { AbstractQuestionEntity } from './entities/question/AbstractQuestionEntity';
import { FreeTextQuestionEntity } from './entities/question/FreeTextQuestionEntity';
import { MultipleChoiceQuestionEntity } from './entities/question/MultipleChoiceQuestionEntity';
import { RangedQuestionEntity } from './entities/question/RangedQuestionEntity';
import { SingleChoiceQuestionEntity } from './entities/question/SingleChoiceQuestionEntity';
import { SurveyQuestionEntity } from './entities/question/SurveyQuestionEntity';
import { TrueFalseSingleChoiceQuestionEntity } from './entities/question/TrueFalseSingleChoiceQuestionEntity';
import { YesNoSingleChoiceQuestionEntity } from './entities/question/YesNoSingleChoiceQuestionEntity';
import { QuestionType } from './enums/QuestionType';

export const getQuestionForType = (settingsService, type: QuestionType, data = {}): AbstractQuestionEntity => {
  switch (type) {
    case QuestionType.FreeTextQuestion:
      return new FreeTextQuestionEntity(settingsService, data);
    case QuestionType.ABCDSurveyQuestion:
      return new ABCDSurveyQuestionEntity(settingsService, data);
    case QuestionType.YesNoSingleChoiceQuestion:
      return new YesNoSingleChoiceQuestionEntity(settingsService, data);
    case QuestionType.TrueFalseSingleChoiceQuestion:
      return new TrueFalseSingleChoiceQuestionEntity(settingsService, data);
    case QuestionType.SingleChoiceQuestion:
      return new SingleChoiceQuestionEntity(settingsService, data);
    case QuestionType.MultipleChoiceQuestion:
      return new MultipleChoiceQuestionEntity(settingsService, data);
    case QuestionType.RangedQuestion:
      return new RangedQuestionEntity(settingsService, data);
    case QuestionType.SurveyQuestion:
      return new SurveyQuestionEntity(settingsService, data);
    default:
      throw new Error(`Cannot build question with type: ${type}`);
  }
};

export const getDefaultQuestionForType = (settingsService, translateService: TranslateService, type: QuestionType, data = {}) => {
  switch (type) {
    case QuestionType.TrueFalseSingleChoiceQuestion:
      return new TrueFalseSingleChoiceQuestionEntity(settingsService, {
        ...data,
        answerOptionList: [
          new DefaultAnswerEntity(settingsService, {
            answerText: translateService.instant('global.true'),
            isCorrect: false,
          }), new DefaultAnswerEntity(settingsService, {
            answerText: translateService.instant('global.false'),
            isCorrect: false,
          }),
        ],
      });
    case QuestionType.YesNoSingleChoiceQuestion:
      return new YesNoSingleChoiceQuestionEntity(settingsService, {
        ...data,
        answerOptionList: [
          new DefaultAnswerEntity(settingsService, {
            answerText: translateService.instant('global.yes'),
            isCorrect: false,
          }), new DefaultAnswerEntity(settingsService, {
            answerText: translateService.instant('global.no'),
            isCorrect: false,
          }),
        ],
      });
    case QuestionType.ABCDSurveyQuestion:
      return new ABCDSurveyQuestionEntity(settingsService, {
        ...data,
        answerOptionList: [
          new DefaultAnswerEntity(settingsService, {
            answerText: 'A',
            isCorrect: false,
          }), new DefaultAnswerEntity(settingsService, {
            answerText: 'B',
            isCorrect: false,
          }), new DefaultAnswerEntity(settingsService, {
            answerText: 'C',
            isCorrect: false,
          }), new DefaultAnswerEntity(settingsService, {
            answerText: 'D',
            isCorrect: false,
          }),
        ],
      });
    case QuestionType.FreeTextQuestion:
      return new FreeTextQuestionEntity(settingsService, {
        ...data,
        answerOptionList: [
          new FreeTextAnswerEntity(settingsService, {}),
        ],
      });
    default:
      return getQuestionForType(settingsService, type, data);
  }
};
