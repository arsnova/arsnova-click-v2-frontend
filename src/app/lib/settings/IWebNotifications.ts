export interface IWebNotifications {
    vapidPublicKey: string;
    vapidPrivateKey: string;
}
