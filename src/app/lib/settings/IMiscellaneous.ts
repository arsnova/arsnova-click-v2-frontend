export interface IMiscellaneous {
    chromiumPath: string;
    projectEmail: string;
    logoFilename: string;
}
