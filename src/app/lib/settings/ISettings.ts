import {IEnvironment} from './IEnvironment';
import {IGit} from './IGit';
import {IMiscellaneous} from './IMiscellaneous';
import {ITwitter} from './ITwitter';
import {IWebNotifications} from './IWebNotifications';

export interface ISettings {
    _id?: string;
    configName: string;
    config: IEnvironment & IGit & IMiscellaneous & ITwitter & IWebNotifications;
}
