export interface IEnvironment {
  production: boolean;
  serverEndpoint: string;
  stompConfig: {
    endpoint: string, user: string, password: string, vhost: string,
  };
  version: string;
}
