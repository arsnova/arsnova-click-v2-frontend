import { AbstractAnswerEntity } from './entities/answer/AbstractAnswerEntity';
import { DefaultAnswerEntity } from './entities/answer/DefaultAnswerEntity';
import { FreeTextAnswerEntity } from './entities/answer/FreetextAnwerEntity';
import { AnswerType } from './enums/AnswerType';

export const getAnswerForType = (settingsService, type: AnswerType, data?: object): AbstractAnswerEntity => {
  switch (type) {
    case AnswerType.DefaultAnswerOption:
      return new DefaultAnswerEntity(settingsService, data);
    case AnswerType.FreeTextAnswerOption:
      return new FreeTextAnswerEntity(settingsService, data);
    default:
      throw new Error(`Cannot build answer with type: ${type}`);
  }
};
