export const hexToRgb = (hex): { r: number, g: number, b: number } => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  } : null;
};

export const transformForegroundColor = (rgbObj: { r: number, g: number, b: number }): string => {
  const o = Math.round((
                         (
                           rgbObj.r * 299
                         ) + (
                           rgbObj.g * 587
                         ) + (
                           rgbObj.b * 114
                         )
                       ) / 1000);
  return o < 125 ? 'ffffff' : '000000';
};

export const ColorTransform = { hexToRgb, transformForegroundColor };
