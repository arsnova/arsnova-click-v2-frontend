export enum CountdownState {
    Running         = 'Running',
    Paused          = 'Paused',
    Stopped         = 'Stopped',
}