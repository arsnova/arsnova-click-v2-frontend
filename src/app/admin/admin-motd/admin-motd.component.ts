import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MarkdownService } from 'ngx-markdown';
import { IMotdEntity } from '../../lib/entities/motd/MotdEntity';
import { EditMotdModalComponent } from '../../modals/edit-motd-modal/edit-motd-modal.component';
import { AdminApiService } from '../../service/api/admin/admin-api.service';
import { FooterBarService } from '../../service/footer-bar/footer-bar.service';
import { MotdDataService } from '../../service/motd/motd-data.service';

@Component({
  selector: 'app-admin-motd',
  templateUrl: './admin-motd.component.html',
  styleUrls: ['./admin-motd.component.scss'],
})
export class AdminMotdComponent implements OnInit {
  @ViewChild('textarea', { static: true }) private textarea: ElementRef<HTMLTextAreaElement>;

  public motdForm: FormGroup;
  public isSending = false;
  public compiledMDContent: String;

  constructor(
    public motdData: MotdDataService,
    private fb: FormBuilder,
    private adminApiService: AdminApiService,
    private ngbModal: NgbModal,
    private markdownService: MarkdownService,
    private datePipe: DatePipe,
    private footerBarService: FooterBarService,
  ) {
    this.updateFooterElements();
  }

  public ngOnInit(): void {
    this.motdForm = this.fb.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      isPinned: [false],
      expireAt: [
        this.datePipe.transform(new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 30), 'yyyy-MM-dd'),
        Validators.required,
      ],
    }, { updateOn: 'change' });
  }

  public submitForm(): void {
    this.isSending = true;
    for (const control of Object.values(this.motdForm.controls)) {
      control.markAsDirty();
      control.updateValueAndValidity();
      if (control.invalid) {
        console.log(control);
      }
    }

    if (this.motdForm.valid) {
      const newMotd: IMotdEntity = {
        title: this.motdForm.get('title').value,
        content: this.motdForm.get('content').value,
        isPinned: this.motdForm.get('isPinned').value,
        expireAt: this.motdForm.get('expireAt').value,
      };
      this.adminApiService.createMotd(newMotd).subscribe(motd => {
        if (motd) {
          this.motdData.allMotds = [...this.motdData.allMotds, motd];
          this.motdForm.reset();
        } else {
          console.log('error creating motd');
        }
        this.isSending = false;
      });
    }
    this.isSending = false;
  }

  public deleteMotd(motdId: String): void {
    this.adminApiService.deleteMotd(motdId).subscribe(res => {
      if (res) {
        this.motdData.allMotds = this.motdData.allMotds.filter(m => m._id !== motdId);
      }
    });
  }

  public startEditMotd(motd: IMotdEntity): void {
    const editMotdRef = this.ngbModal.open(EditMotdModalComponent);
    editMotdRef.componentInstance.motd = motd;
    editMotdRef.componentInstance.createForm();
  }

  public compileMD(motdContent: string): string {
    return this.markdownService.compile(motdContent);
  }

  public getMarkdownPreview(): string {
    return this.markdownService.compile(this.motdForm.get('content').value);
  }

  public isPinnedChange(status: boolean): void {
    if (status) {
      this.motdForm.get('expireAt')
        .setValue(this.datePipe.transform(new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1000), 'yyyy-MM-dd'));
    } else {
      this.motdForm.get('expireAt')
        .setValue(this.datePipe.transform(new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 30), 'yyyy-MM-dd'));
    }
  }

  private updateFooterElements(): void {
    const footerElements = [
      this.footerBarService.footerElemBack,
    ];

    this.footerBarService.replaceFooterElements(footerElements);
  }
}
