import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PipesModule } from '../pipes/pipes.module';
import { SharedModule } from '../shared/shared.module';
import { AdminOverviewComponent } from './admin-overview/admin-overview.component';
import { QuizAdminComponent } from './quiz-admin/quiz-admin.component';
import { QuizDetailsAdminComponent } from './quiz-details-admin/quiz-details-admin.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { QuizPoolAdminComponent } from './quiz-pool-admin/quiz-pool-admin.component';
import { AdminMotdComponent } from './admin-motd/admin-motd.component';
import { AdminSettingsComponent } from './admin-settings/admin-settings.component';
import {ReactiveFormsModule} from '@angular/forms';
import { TwitterSettingsFormComponent } from './admin-settings/twitter-settings-form/twitter-settings-form.component';
import { FrontEnvSettingsFormComponent } from './admin-settings/front-env-settings-form/front-env-settings-form.component';
import { AdminMiscSettingsFormComponent } from './admin-settings/admin-misc-settings-form/admin-misc-settings-form.component';
import { AdminGitSettingsFormComponent } from './admin-settings/admin-git-settings-form/admin-git-settings-form.component';
import { AdminWebNotificationSettingsFormComponent } from './admin-settings/admin-web-notification-settings-form/admin-web-notification-settings-form.component';

const routes: Routes = [
  {
    path: 'user',
    component: UserAdminComponent,
  },
  {
    path: 'quiz',
    component: QuizAdminComponent,
  },
  {
    path: 'motd',
    component: AdminMotdComponent,
  },
  {
    path: 'settings',
    component: AdminSettingsComponent,
  },
  {
    path: 'quiz/pool',
    component: QuizPoolAdminComponent,
  },
  {
    path: 'quiz/:id',
    component: QuizDetailsAdminComponent,
  },
  {
    path: '',
    component: AdminOverviewComponent,
  },
];

@NgModule({
  declarations: [UserAdminComponent, QuizAdminComponent, AdminOverviewComponent, QuizDetailsAdminComponent, QuizPoolAdminComponent, AdminMotdComponent, AdminSettingsComponent, TwitterSettingsFormComponent, FrontEnvSettingsFormComponent, AdminMiscSettingsFormComponent, AdminGitSettingsFormComponent, AdminWebNotificationSettingsFormComponent],
  imports: [
    SharedModule, RouterModule.forChild(routes), PipesModule, ReactiveFormsModule,
  ],
})
export class AdminModule {
}
