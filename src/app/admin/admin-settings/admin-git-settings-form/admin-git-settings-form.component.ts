import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdminApiService} from '../../../service/api/admin/admin-api.service';
import {AdminSettingsApiService} from '../../../service/api/admin/admin-settings-api.service';

@Component({
  selector: 'app-admin-git-settings-form',
  templateUrl: './admin-git-settings-form.component.html',
  styleUrls: ['./admin-git-settings-form.component.scss']
})
export class AdminGitSettingsFormComponent implements OnInit {
  public settingsForm: FormGroup;
  public settings: any;
  public isSending = false;

  constructor(private fb: FormBuilder,
              private adminApiService: AdminApiService,
              private adminSettingsApi: AdminSettingsApiService
  ) {
  }

  public ngOnInit(): void {
    this.adminSettingsApi.getSettings('git').subscribe(settings => {
      if (settings) {
        this.settings = settings;
        this.settingsForm = this.fb.group({
          frontendGitlabId: [settings.config.frontendGitlabId, []],
          backendGitlabId: [settings.config.backendGitlabId, []],
          gitlabLoginToken: [settings.config.gitlabLoginToken, []],
          gitlabHost: [settings.config.gitlabHost, []],
          gitlabTargetBranch: [settings.config.gitlabTargetBranch, []],
        });
      } else {
        console.log('no settings received');
      }
    });
  }

  public async submitForm(): Promise<void> {
    this.isSending = true;
    for (const control of Object.values(this.settingsForm.controls)) {
      control.markAsDirty();
      control.updateValueAndValidity();
      if (control.invalid) {
        console.log(control);
      }
    }

    if (this.settingsForm.valid) {
      this.settings.config = {...this.settings.config, ...this.settingsForm.getRawValue()};
      this.adminApiService.editSettings(this.settings).subscribe(res => {
        if (res) {
          this.adminSettingsApi.twitterSettings = res.config;
        } else {
          console.log('error udpating settings');
        }
        this.isSending = false;
      });
    }
  }


}
