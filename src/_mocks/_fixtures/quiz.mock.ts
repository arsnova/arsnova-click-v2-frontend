import { QuizEntity } from '../../app/lib/entities/QuizEntity';
import { MusicSessionConfigurationEntity } from '../../app/lib/entities/session-configuration/MusicSessionConfigurationEntity';
import { NickSessionConfigurationEntity } from '../../app/lib/entities/session-configuration/NickSessionConfigurationEntity';
import { SessionConfigurationEntity } from '../../app/lib/entities/session-configuration/SessionConfigurationEntity';
import { QuizState } from '../../app/lib/enums/QuizState';
import { QuizVisibility } from '../../app/lib/enums/QuizVisibility';
import {SettingsMockService} from '../../app/service/settings/settings.mock.service';

const settingsService: any = new SettingsMockService(null);

export const QuizMock: QuizEntity = new QuizEntity(settingsService, {
  currentQuestionIndex: 0,
  currentStartTimestamp: 0,
  description: '',
  expiry: undefined,
  memberGroups: undefined,
  questionList: undefined,
  sessionConfig: new SessionConfigurationEntity(settingsService, {
    nicks: new NickSessionConfigurationEntity(settingsService, { selectedNicks: [] }),
    music: new MusicSessionConfigurationEntity(settingsService, {}),
  }),
  state: QuizState.Active,
  visibility: QuizVisibility.Account,
  name: 'quiz-test',
});
