import { IEnvironment } from '../app/lib/interfaces/IEnvironment';

export const environment: IEnvironment = {
  production: false,
  serverEndpoint: 'http://localhost:3010',
  stompConfig: {
    endpoint: 'ws://localhost:15674/ws',
    user: 'user',
    password: 'bitnami',
    vhost: '/',
  },
  version: '__VERSION__',
};
