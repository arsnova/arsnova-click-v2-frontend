#!/usr/bin/env bash

echo "Installing npm modules"
npm install
if [[ $? -ne "0" ]]
then
  exit 1
fi

echo "------------"
echo
echo "Temporary fix Mousetrap Instance Issue"
echo
echo
echo "REMOVE AFTER PACKAGE MAINTAINER HAS MERGED THE FIX"
echo
echo "------------"
sed -i "s|import 'mousetrap';|import { MousetrapInstance } from 'mousetrap';|g" "node_modules/angular2-hotkeys/lib/hotkeys.service.d.ts"

echo "Building unique version hash for the build"
HASH=$(date | md5sum | head -c32)
echo $HASH > src/assets/version.txt
sed -i s/__VERSION__/$HASH/ src/environments/environment*.ts

echo "Building the app"
npm run build:"$1"
if [[ $? -ne "0" ]]
then
  exit 2
fi

. ./build-assets.sh "$1" "$2"

echo "Gzipping app files"
cd /usr/src/app/dist/frontend/browser
find . -name "*.*" -type f -print0 | xargs -0 gzip -9 -k
